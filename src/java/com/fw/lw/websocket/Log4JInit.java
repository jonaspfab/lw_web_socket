package com.fw.lw.websocket;
    
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author salih.canoz
 */
public class Log4JInit implements ServletContextListener {

    private static Properties log4jProperties = null;

    private void initLogging() {
        if (log4jProperties == null) {
            InputStream in = null;
            try {
                log4jProperties = new Properties();
                in = getClass().getResourceAsStream("/log4j.properties");
                log4jProperties.load(in);
                PropertyConfigurator.configure(log4jProperties);
                //System.out.println("Log4j initialized");
            } catch (Exception e) {
                //System.out.println("Error configuring log4j " + e.getMessage());
                e.printStackTrace();
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        initLogging();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log4jProperties = null;
    }
}
