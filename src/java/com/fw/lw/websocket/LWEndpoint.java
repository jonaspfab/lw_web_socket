package com.fw.lw.websocket;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.apache.log4j.Logger;

/**
 *
 * @author salih.canoz
 */
@ServerEndpoint("/endpoint")
public class LWEndpoint {

    public static final int WEB_MESSAGE_ON_OPEN = 0;
    public static final int WEB_MESSAGE_ALREADY_LOGGED_IN = 1000;

    private static final Logger LOG = Logger.getLogger(LWEndpoint.class);

    private static final ConcurrentHashMap<String, Session> SESSIONS = new ConcurrentHashMap<>();

    private static final ConcurrentHashMap<Integer, Session> ACCOUNTS = new ConcurrentHashMap<>();

    private static final ConcurrentHashMap<Integer, Session> PARTNERS = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<Integer, Boolean> ARC_ADMIN = new ConcurrentHashMap<>();

    /**
     * Incoming message is represented as parameter and return value is going to
     * be send back to peer.
     *
     * {@link javax.websocket.Session} can be put as a parameter and then you
     * can gain better control of whole communication, like sending more than
     * one message, process path parameters,
     * {@link javax.websocket.Extension Extensions} and sub-protocols and much
     * more.
     *
     * @param message incoming text message.
     * @param session
     * @return outgoing message.
     *
     * @see OnMessage
     * @see javax.websocket.Session
     */
    @OnMessage
    public synchronized String onMessage(String message, Session session) {
        //LOG.info("message-->" + message);
        if (message != null && !message.equals("")) {
            WebMessage webMessage = new Gson().fromJson(message, WebMessage.class);
            //LOG.info("webMessage-->" + webMessage);
            Integer accountId = webMessage.getAccount();
            //LOG.info("inside onMessage from server end point accountId--->" + accountId);
            //LOG.info("inside onMessage from server end point message--->" + message);
            Integer partnerId = webMessage.getPartner();
            boolean isARCEnabled = webMessage.isIsARCEnabled();
            boolean sendWebSocketMsg = webMessage.isSendWebSocketMsg();
            boolean isARCAdmin = webMessage.isIsArcAdmin();
            for (String s : SESSIONS.keySet()) {
                LOG.info("key of ACTIVE SESSIONS--->" + s);
            }
            for (String s : SESSIONS.keySet()) {
                //LOG.info("key  of SESSION--->" + s);
                //LOG.info("session id--->" + session.getId());
                if (!ACCOUNTS.containsKey(accountId)) {
                    ACCOUNTS.put(webMessage.getAccount(), session);
                }
                if (!PARTNERS.containsKey(partnerId)) {
                    PARTNERS.put(webMessage.getPartner(), session);
                }
                if (!ARC_ADMIN.containsKey(accountId)) {
                    ARC_ADMIN.put(accountId, isARCAdmin);
                }
                if (session.getId().equals(s)) {
                    continue;
                }
                try {
                    if (webMessage.getType() == WEB_MESSAGE_ON_OPEN) {
                        Session sessionAcc = ACCOUNTS.get(accountId);
                        if (ACCOUNTS.containsKey(accountId) && !session.equals(sessionAcc)) {
                            Session accountSession = ACCOUNTS.get(webMessage.getAccount());
                            if (accountSession != null) {
                                WebMessage webMessage2 = new WebMessage(accountId, null, "logged somewhere else", WEB_MESSAGE_ALREADY_LOGGED_IN, null, null, false, true, isARCAdmin);
                                try {
                                    if(accountSession.isOpen()){
                                        accountSession.getBasicRemote().sendText(webMessage2.json());
                                    }
                                    else{
                                        ACCOUNTS.remove(webMessage.getAccount());
                                    }
                                } catch (IOException ex) {
                                    LOG.error("error=", ex);
                                }
                            }
                        }
                        ACCOUNTS.put(webMessage.getAccount(), session);
                        PARTNERS.put(webMessage.getPartner(), session);
                        ARC_ADMIN.put(accountId, isARCAdmin);
                    } else {
                        //Send websocket msg to ARC Admin too
                        for (Integer accountIdLocal : ACCOUNTS.keySet()) {
                            //LOG.info("For AccountId--->" + accountIdLocal);
                            if (!ARC_ADMIN.isEmpty()) {
                                boolean isArcAdminVal = ARC_ADMIN.get(accountIdLocal);
                                //LOG.info("1st isArcAdminVal--->" + isArcAdminVal);
                                if (isArcAdminVal) {
                                    //LOG.info(" If isArcAdminVal--->");
                                    if (isARCEnabled && sendWebSocketMsg) {
                                        //LOG.info("ARC is enabled and sendWebSocketMsg is true--->");
                                        for (Integer partnerIdLocal : PARTNERS.keySet()) {
                                            if (partnerIdLocal.equals(webMessage.getPartner())) {
                                                Session accountSession = ACCOUNTS.get(accountIdLocal);
                                                if (accountSession != null) {
                                                    //LOG.info("before sendText for ARC Admin webMessage--->" + webMessage.json());
                                                    if(accountSession.isOpen()){
                                                        accountSession.getBasicRemote().sendText(webMessage.json());
                                                    }else{
                                                        ACCOUNTS.remove(accountIdLocal);
                                                    }
                                                    //LOG.info("after sendText for ARC Admin webMessage--->" + webMessage.json());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // from device to panel
                        if (!ARC_ADMIN.isEmpty()) {
                            boolean isArcAdminVal = ARC_ADMIN.get(webMessage.getAccount());
                            //LOG.info("2nd isArcAdminVal--->" + isArcAdminVal);
                            if (!isArcAdminVal) {
                                //LOG.info("If NOT isArcAdminVal-->");
                                //LOG.info("webMessage.getAccount()-->" + webMessage.getAccount());
                                Session accountSession = ACCOUNTS.get(webMessage.getAccount());
                                if (accountSession != null) {
                                    //LOG.info("accountSession.getId() to which msg is sent-->" + accountSession.getId());
                                    //LOG.info("before sendText for NOT ARC Admin webMessage-->" + webMessage.json());
                                    if(accountSession.isOpen()){
                                        accountSession.getBasicRemote().sendText(webMessage.json());
                                    }else{
                                        ACCOUNTS.remove(webMessage.getAccount());
                                    }
                                    //LOG.info("after sendText for NOT ARC Admin webMessage-->" + webMessage.json());
                                    break;
                                }
                            }
                        } else {
                            Session accountSession = ACCOUNTS.get(webMessage.getAccount());
                            //LOG.info("webMessage.getAccount() inside else-->" + webMessage.getAccount());
                            if (accountSession != null) {
                                //LOG.info("before sendText inside else-->" + webMessage.json());
                                if(accountSession.isOpen()){
                                    accountSession.getBasicRemote().sendText(webMessage.json());
                                }else{
                                    ACCOUNTS.remove(webMessage.getAccount());
                                }
                                //LOG.info("after sendText inside else-->" + webMessage.json());
                                break;
                            }
                        }

                    }

                } catch (Exception ex) {
                    LOG.error("error", ex);
                }
            }
        }
        return message;
    }

    @OnOpen
    public synchronized void onOpen(Session session) {
        //LOG.info("inside onOpen from server end point session id --->" + session.getId());
        if (!SESSIONS.containsKey(session.getId())) {
            SESSIONS.put(session.getId(), session);
        }
    }

    @OnClose
    public synchronized void onClose(Session session) {
        //LOG.info("inside onClose reason --->" + reason);
        LOG.info("inside onClose from server end point session id --->" + session.getId());
        if (SESSIONS.containsKey(session.getId())) {
            SESSIONS.remove(session.getId());
        }

        Integer accountId = null;
        Set<Integer> keys = ACCOUNTS.keySet();

        for (Integer i : keys) {
            Session s = ACCOUNTS.get(i);
            if (session.equals(s)) {
                accountId = i;
                break;
            }
        }

        if (accountId != null) {
            ACCOUNTS.remove(accountId);
        }
    }

    @OnError
    public void onError(Session session, Throwable t
    ) {
        LOG.error("error for session " + session.getId(), t);
    }
}
