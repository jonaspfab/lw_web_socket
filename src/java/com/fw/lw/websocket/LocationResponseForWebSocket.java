package com.fw.lw.websocket;

import com.fw.lw.ejb.jpa.Location;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Saloni.Agarwal
 */
public class LocationResponseForWebSocket {

    private Long id;
    private short reportCause;
    private Integer accuracy;
    private Short speed;
    private Short direction;
    private short fixStatus;
    private Short battery;
    private Date fixTime;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private Short altitude;
    private String address;
    private Integer notificationEvent;
    private Long cancelledLocId;

    public LocationResponseForWebSocket(Location location) {
        id = location.getId();
        reportCause = location.getReportCause();
        accuracy = location.getAccuracy();
        speed = location.getSpeed();
        direction = location.getDirection();
        fixStatus = location.getFixStatus();
        battery = location.getBattery();
        fixTime = location.getFixTime();
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        altitude = location.getAltitude();
        address = location.getAddress();
        notificationEvent = location.getFkNotificationEvent().getId();
    }

    public LocationResponseForWebSocket(Location oldlocation, Location newLocation) {
        id = oldlocation.getId();
        cancelledLocId = newLocation.getId();
        notificationEvent = newLocation.getFkNotificationEvent().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public short getReportCause() {
        return reportCause;
    }

    public void setReportCause(short reportCause) {
        this.reportCause = reportCause;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public Short getSpeed() {
        return speed;
    }

    public void setSpeed(Short speed) {
        this.speed = speed;
    }

    public Short getDirection() {
        return direction;
    }

    public void setDirection(Short direction) {
        this.direction = direction;
    }

    public short getFixStatus() {
        return fixStatus;
    }

    public void setFixStatus(short fixStatus) {
        this.fixStatus = fixStatus;
    }

    public Short getBattery() {
        return battery;
    }

    public void setBattery(Short battery) {
        this.battery = battery;
    }

    public Date getFixTime() {
        return fixTime;
    }

    public void setFixTime(Date fixTime) {
        this.fixTime = fixTime;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public Short getAltitude() {
        return altitude;
    }

    public void setAltitude(Short altitude) {
        this.altitude = altitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getNotificationEvent() {
        return notificationEvent;
    }

    public void setNotificationEvent(Integer notificationEvent) {
        this.notificationEvent = notificationEvent;
    }

    public Long getCancelledLocId() {
        return cancelledLocId;
    }

    public void setCancelledLocId(Long cancelledLocId) {
        this.cancelledLocId = cancelledLocId;
    }

}
