/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fw.lw.websocket;

import java.io.StringReader;
import java.io.StringWriter;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.apache.log4j.Logger;

/**
 *
 * @author fasiha.anjum
 */
@ServerEndpoint("/webrtcendpoint")
public class WebRTCEndPoint {

    private static final Logger LOG = Logger.getLogger(WebRTCEndPoint.class);

    /**
     * The room bean singleton.
     */
    private final WebRTCRoomBean roomBean = WebRTCRoomBean.getRoomBean();

    /**
     * The onclose is used to send "bye" to the partner in the room.
     *
     * @param session The session who is closing
     */
    @OnClose
    public void onClose(Session session) {
        roomBean.disconnect(session);
    }

    /**
     * The real method that handles with the messages.
     *
     * @param message The message of the client
     * @param session The client session
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        try (JsonReader reader = Json.createReader(new StringReader(message))) {
            JsonObject data = (JsonObject) reader.read();
            String type = data.getString("type");
            switch (type) {
                case "join":
                    // connect the user to the room
                    String roomKey = data.getString("roomKey");
                    String username = data.getString("username");
                    roomBean.connect(roomKey, username, session);
                    break;
                case "bye":
                    // disconnect the user
                    roomBean.disconnect(session);
                    break;
                    
                    case "isWaiting":
                    // disconnect the user
                    roomBean.disconnect(session);
                    break;
                default:
                    // we have to deal with the message and send it to the other user
                    roomBean.sendToTheOther(session, data);
                    break;
            }
        }
        catch (Exception e) {
            LOG.error(e);
        }
    }

    /**
     * Helper to close a websocket session.
     *
     * @param s The session to close
     */
    static public void closeSession(Session s) {
        try {
            if (s != null) {
                s.close();
            }
        }
        catch (Exception e) {
            LOG.error(e);
        }
    }

    /**
     * Helper to send a a JSON message to another session.
     *
     * @param session The session to send the message to
     * @param data The data to send
     */
    static public void send(Session session, JsonObject data) {
        if (session != null && data != null) {
            StringWriter stWriter = new StringWriter();
            try (JsonWriter jsonWriter = Json.createWriter(stWriter)) {
                jsonWriter.writeObject(data);
            }
            catch (Exception e) {
                LOG.error(e);
            }
            try {
                session.getBasicRemote().sendObject(stWriter.toString());
            }
            catch (Exception e) {
                LOG.error(e);
            }
        }
    }

    @OnOpen
    public void onOpen() {
        LOG.info("opening web rtc");
    }

    @OnError
    public void onError(Throwable t) {
        LOG.error(t);
    }

}
