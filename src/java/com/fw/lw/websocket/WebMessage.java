package com.fw.lw.websocket;

import com.google.gson.Gson;
import java.io.Serializable;

/**
 *
 * @author salih.canoz
 */
public class WebMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer account;
    private Integer device;
    private String message;
    private int type;
    private LocationResponseForWebSocket location;
    private String alias;
    private Integer partner;
    private boolean isARCEnabled;
    private boolean sendWebSocketMsg;
    private boolean isArcAdmin;

    public WebMessage(Integer account, Integer device, String message, int type, String alias, Integer partner, boolean isARCEnabled, boolean sendWebSocketMsg, boolean isArcAdmin) {
        this.account = account;
        this.device = device;
        this.message = message;
        this.type = type;
        this.alias = alias;
        this.partner = partner;
        this.isARCEnabled = isARCEnabled;
        this.sendWebSocketMsg = sendWebSocketMsg;
        this.isArcAdmin = isArcAdmin;
    }

    public WebMessage(Integer device, String message, int type, String alias) {
        this.device = device;
        this.message = message;
        this.type = type;
        this.alias = alias;
    }

    public Integer getAccount() {
        return account;
    }

    public void setAccount(Integer account) {
        this.account = account;
    }

    public Integer getDevice() {
        return device;
    }

    public void setDevice(Integer device) {
        this.device = device;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public LocationResponseForWebSocket getLocation() {
        return location;
    }

    public void setLocation(LocationResponseForWebSocket location) {
        this.location = location;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Integer getPartner() {
        return partner;
    }

    public void setPartner(Integer partner) {
        this.partner = partner;
    }

    public boolean isIsARCEnabled() {
        return isARCEnabled;
    }

    public void setIsARCEnabled(boolean isARCEnabled) {
        this.isARCEnabled = isARCEnabled;
    }

    public boolean isSendWebSocketMsg() {
        return sendWebSocketMsg;
    }

    public void setSendWebSocketMsg(boolean sendWebSocketMsg) {
        this.sendWebSocketMsg = sendWebSocketMsg;
    }

    public boolean isIsArcAdmin() {
        return isArcAdmin;
    }

    public void setIsArcAdmin(boolean isArcAdmin) {
        this.isArcAdmin = isArcAdmin;
    }

    public String json() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

}
